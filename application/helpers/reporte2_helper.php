<?php

function setReporteCSV($filecreator, $filetitle, $filename, $data, $sheettitle) {
    $objPHPExcel = new PHPExcel();
// Establecer propiedades
    $objPHPExcel->getProperties()
            ->setCreator($filecreator)
            ->setTitle($filetitle);

// Agregar Titulo de las Columnas
    $objPHPExcel->getActiveSheet()
            ->fromArray(
                    $data, // The data to set
                    NULL, // Array values with this value will not be set
                    'A1'        // 'A1' Top left coordinate of the worksheet range where
                    //    we want to set these values (default is A1)
    );


// Renombrar Hoja
    $objPHPExcel->getActiveSheet()->setTitle($sheettitle);


// Establecer la hoja activa, para que cuando se abra el documento se muestre primero.
    $objPHPExcel->setActiveSheetIndex(0);

    $objWriter = new PHPExcel_Writer_CSV($objPHPExcel);
    $objWriter->setDelimiter(';');
    $objWriter->setEnclosure('');
    $objWriter->setLineEnding("\r\n");
    $objWriter->setSheetIndex(0);
    $objWriter->save('Test.csv');
    $objWriter->save($filename.".csv");


    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="'.$filename.'.csv"');
    header('Cache-Control: max-age=0');

    $objWriter->save('php://output');


    exit;
}
