<?php

function setReporteExcel($filecreator, $filetitle, $filename, $columnstitles, $data, $sheettitle, $celdastitulo, $starttitle, $startcell, $reportType, $numcols, $fecha) {
    $objPHPExcel = new PHPExcel();
// Establecer propiedades
    $objPHPExcel->getProperties()
            ->setCreator($filecreator)
            ->setTitle($filetitle);

// Agregar Titulo de las Columnas
    $objPHPExcel->getActiveSheet()
            ->fromArray(
                    $columnstitles, // The data to set
                    NULL, // Array values with this value will not be set
                    $starttitle         // 'A1' Top left coordinate of the worksheet range where
                    //    we want to set these values (default is A1)
    );

// Agregar Datos en las Filas
    $objPHPExcel->getActiveSheet()
            ->fromArray(
                    $data, // The data to set
                    NULL, // Array values with this value will not be set
                    $startcell         // 'A2' Top left coordinate of the worksheet range where
                    //    we want to set these values (default is A1)
    );
// Agregar Datos Individuales
    // $objPHPExcel->getActiveSheet()->SetCellValue('B2', 'Hola mundo');
    // 
// Renombrar Hoja
    $objPHPExcel->getActiveSheet()->setTitle($sheettitle);

    //Fuente Negrita
    $objPHPExcel->getActiveSheet()->getStyle($celdastitulo)->getFont()->setBold(true);

    //Quitar Líneas de Cuadricula
    $objPHPExcel->getActiveSheet()->setShowGridlines(false);

// Establecer la hoja activa, para que cuando se abra el documento se muestre primero.
    $objPHPExcel->setActiveSheetIndex(0);

//Establecer color de fondo a Celdas
    $objPHPExcel->getActiveSheet()->getStyle($celdastitulo)->applyFromArray(
            array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => '4ca64c')
                )
            )
    );

//Establecer Width Autosize
    for ($col = 'A'; $col != 'AZ'; $col++) {
        $objPHPExcel->getActiveSheet()
                ->getColumnDimension($col)
                ->setAutoSize(true);
    }

    $styleArray = array(
        'borders' => array(
            'allborders' => array(
                'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
                'color' => array('rgb' => '000000')
            )
        )
    );

    $styleArray2 = array(
        'borders' => array(
            'allborders' => array(
                'style' => PHPExcel_Style_Border::BORDER_THIN,
                'color' => array('rgb' => '000000')
            )
        )
    );

    $font1 = array(
        'font' => array(
            'bold' => true,
//                'color' => array('rgb' => 'FF0000'),
            'size' => 26,
//                'name' => 'Verdana'
    ));

    $font2 = array(
        'font' => array(
            'bold' => true,
//                'color' => array('rgb' => 'FF0000'),
            'size' => 20,
//                'name' => 'Verdana'
    ));

    $font3 = array(
        'font' => array(
            'bold' => true,
//                'color' => array('rgb' => 'FF0000'),
            'size' => 18,
//                'name' => 'Verdana'
    ));

    $style = array(
        'alignment' => array(
            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
        )
    );

//Condiciones Reporte Historico
    if ($reportType == 1) {       
        $objPHPExcel->getActiveSheet()->getStyle($celdastitulo)->applyFromArray($styleArray);
        unset($styleArray);

        $range = "A11:AK" . ($numcols + 10);
        $objPHPExcel->getActiveSheet()->getStyle($range)->applyFromArray($styleArray2);
        unset($styleArray2);

        $objPHPExcel->getActiveSheet()->mergeCells('A8:D8');
        $objPHPExcel->getActiveSheet()->mergeCells('D3:J3');
        $objPHPExcel->getActiveSheet()->mergeCells('D5:J5');
        $objPHPExcel->getActiveSheet()->mergeCells('D6:J6');

        $objPHPExcel->getActiveSheet()->SetCellValue('D3', 'DISTRIBUCIONES LA PRINCIPAL LA GRITA, C.A.');
        $objPHPExcel->getActiveSheet()->SetCellValue('D5', 'Informe Histórico de Pagos a Fleteros');
        $objPHPExcel->getActiveSheet()->SetCellValue('D6', 'Sucursal La Grita');
        $objPHPExcel->getActiveSheet()->SetCellValue('A8', 'Fecha de Reporte: '.$fecha);
        $objPHPExcel->getActiveSheet()->getStyle('A8:D8')->getFont()->setBold(true);

        $objPHPExcel->getActiveSheet()->getStyle('D3')->applyFromArray($font1);
        $objPHPExcel->getActiveSheet()->getStyle('D5')->applyFromArray($font2);
        $objPHPExcel->getActiveSheet()->getStyle('D6')->applyFromArray($font3);

        $objPHPExcel->getActiveSheet()->getStyle("D3:J6")->applyFromArray($style);

//Agregar Imagen al Archivo
        $gdImage = imagecreatefromjpeg('images/principal1.jpg');
// Add a drawing to the worksheetecho date('H:i:s') . " Add a drawing to the worksheet\n";
        $objDrawing = new PHPExcel_Worksheet_MemoryDrawing();
        $objDrawing->setName('Principal-1');
        $objDrawing->setDescription('Principal-1');
        $objDrawing->setImageResource($gdImage);
        $objDrawing->setRenderingFunction(PHPExcel_Worksheet_MemoryDrawing::RENDERING_JPEG);
        $objDrawing->setMimeType(PHPExcel_Worksheet_MemoryDrawing::MIMETYPE_DEFAULT);
        $objDrawing->setHeight(180);
        $objDrawing->setCoordinates('B1');
//    $objDrawing->setCoordinates('L1');
        $objDrawing->setWorksheet($objPHPExcel->getActiveSheet());


        $gdImage2 = imagecreatefromjpeg('images/principal2.jpg');
// Add a drawing to the worksheetecho date('H:i:s') . " Add a drawing to the worksheet\n";
        $objDrawing2 = new PHPExcel_Worksheet_MemoryDrawing();
        $objDrawing2->setName('Principal-2');
        $objDrawing2->setDescription('Principal-2');
        $objDrawing2->setImageResource($gdImage2);
        $objDrawing2->setRenderingFunction(PHPExcel_Worksheet_MemoryDrawing::RENDERING_JPEG);
        $objDrawing2->setMimeType(PHPExcel_Worksheet_MemoryDrawing::MIMETYPE_DEFAULT);
        $objDrawing2->setHeight(180);
        $objDrawing2->setCoordinates('L1');
        $objDrawing2->setWorksheet($objPHPExcel->getActiveSheet());
    }

//Condiciones Reporte Conteo
    if ($reportType == 2) {
        $objPHPExcel->getActiveSheet()->getStyle($celdastitulo)->applyFromArray($styleArray);
        unset($styleArray);

        $range = "A11:H" . ($numcols + 10);
        $objPHPExcel->getActiveSheet()->getStyle($range)->applyFromArray($styleArray2);
        unset($styleArray2);

        $objPHPExcel->getActiveSheet()->mergeCells('A8:B8');
        $objPHPExcel->getActiveSheet()->mergeCells('B4:G4');
        $objPHPExcel->getActiveSheet()->mergeCells('B6:G6');
        $objPHPExcel->getActiveSheet()->mergeCells('B7:G7');

        $objPHPExcel->getActiveSheet()->SetCellValue('B4', 'DISTRIBUCIONES LA PRINCIPAL LA GRITA, C.A.');
        $objPHPExcel->getActiveSheet()->SetCellValue('B6', 'Informe Conteo');
        $objPHPExcel->getActiveSheet()->SetCellValue('B7', 'Sucursal La Grita');
        $objPHPExcel->getActiveSheet()->SetCellValue('A8', 'Fecha de Reporte: ' . $fecha);
        $objPHPExcel->getActiveSheet()->getStyle('A8:B8')->getFont()->setBold(true);

        $objPHPExcel->getActiveSheet()->getStyle('B4')->applyFromArray($font1);
        $objPHPExcel->getActiveSheet()->getStyle('B6')->applyFromArray($font2);
        $objPHPExcel->getActiveSheet()->getStyle('B7')->applyFromArray($font3);

        $objPHPExcel->getActiveSheet()->getStyle("B4:B7")->applyFromArray($style);

        //Agregar Imagen al Archivo
        $gdImage = imagecreatefromjpeg('images/principal1.jpg');
// Add a drawing to the worksheetecho date('H:i:s') . " Add a drawing to the worksheet\n";
        $objDrawing = new PHPExcel_Worksheet_MemoryDrawing();
        $objDrawing->setName('Principal-1');
        $objDrawing->setDescription('Principal-1');
        $objDrawing->setImageResource($gdImage);
        $objDrawing->setRenderingFunction(PHPExcel_Worksheet_MemoryDrawing::RENDERING_JPEG);
        $objDrawing->setMimeType(PHPExcel_Worksheet_MemoryDrawing::MIMETYPE_DEFAULT);
        $objDrawing->setHeight(180);
        $objDrawing->setCoordinates('A1');
        $objDrawing->setWorksheet($objPHPExcel->getActiveSheet());


        $gdImage2 = imagecreatefromjpeg('images/principal2.jpg');
// Add a drawing to the worksheetecho date('H:i:s') . " Add a drawing to the worksheet\n";
        $objDrawing2 = new PHPExcel_Worksheet_MemoryDrawing();
        $objDrawing2->setName('Principal-2');
        $objDrawing2->setDescription('Principal-2');
        $objDrawing2->setImageResource($gdImage2);
        $objDrawing2->setRenderingFunction(PHPExcel_Worksheet_MemoryDrawing::RENDERING_JPEG);
        $objDrawing2->setMimeType(PHPExcel_Worksheet_MemoryDrawing::MIMETYPE_DEFAULT);
        $objDrawing2->setHeight(180);
        $objDrawing2->setCoordinates('F1');
//        $objDrawing2->getOffsetX(100);
//        $objDrawing2->getOffsetY(100);
        $objDrawing2->setWorksheet($objPHPExcel->getActiveSheet());
    }

//Condiciones Reporte Inventario
    if ($reportType == 3) {
        //Combinar Celdas
        $objPHPExcel->getActiveSheet()->mergeCells('D9:E9');
        $objPHPExcel->getActiveSheet()->mergeCells('F9:G9');
        $objPHPExcel->getActiveSheet()->mergeCells('H9:I9');
        $objPHPExcel->getActiveSheet()->mergeCells('J9:K9');
        $objPHPExcel->getActiveSheet()->mergeCells('L9:N9');
        $objPHPExcel->getActiveSheet()->mergeCells('O9:Q9');
        $objPHPExcel->getActiveSheet()->mergeCells('R9:T9');
        $objPHPExcel->getActiveSheet()->mergeCells('A8:C8');
        $objPHPExcel->getActiveSheet()->getStyle('A8:R9')->getFont()->setBold(true);

        $objPHPExcel->getActiveSheet()->SetCellValue('A8', 'Fecha de Reporte: ' . $fecha);
        $objPHPExcel->getActiveSheet()->SetCellValue('D9', 'Deposito 01');
        $objPHPExcel->getActiveSheet()->SetCellValue('F9', 'Deposito 02');
        $objPHPExcel->getActiveSheet()->SetCellValue('H9', 'Guias');
        $objPHPExcel->getActiveSheet()->SetCellValue('J9', 'Facturas');
        $objPHPExcel->getActiveSheet()->SetCellValue('L9', 'Total');
        $objPHPExcel->getActiveSheet()->SetCellValue('O9', 'Conteo Físico');
        $objPHPExcel->getActiveSheet()->SetCellValue('R9', 'Diferencias');

        $objPHPExcel->getActiveSheet()->getStyle('A10:T10')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->getStyle('D9:T9')->applyFromArray($styleArray);
        unset($styleArray);

        $range = "A11:T" . ($numcols + 10);
        $objPHPExcel->getActiveSheet()->getStyle($range)->applyFromArray($styleArray2);
        unset($styleArray2);

        for ($x = 11; $x <= ($numcols + 10); $x++) {
            $objPHPExcel->getActiveSheet()->setCellValue('R' . $x, '=L' . $x . '-O' . $x);
            $objPHPExcel->getActiveSheet()->setCellValue('S' . $x, '=M' . $x . '-P' . $x);
            $objPHPExcel->getActiveSheet()->setCellValue('T' . $x, '=N' . $x . '-Q' . $x);
        }


        $objPHPExcel->getActiveSheet()->mergeCells('C3:O3');
        $objPHPExcel->getActiveSheet()->mergeCells('C5:O5');
        $objPHPExcel->getActiveSheet()->mergeCells('C6:O6');

        $objPHPExcel->getActiveSheet()->SetCellValue('C3', 'DISTRIBUCIONES LA PRINCIPAL LA GRITA, C.A.');
        $objPHPExcel->getActiveSheet()->SetCellValue('C5', 'Informe de Inventario');
        $objPHPExcel->getActiveSheet()->SetCellValue('C6', 'Sucursal La Grita');

        $objPHPExcel->getActiveSheet()->getStyle('C3')->applyFromArray($font1);
        $objPHPExcel->getActiveSheet()->getStyle('C5')->applyFromArray($font2);
        $objPHPExcel->getActiveSheet()->getStyle('C6')->applyFromArray($font3);

        $objPHPExcel->getActiveSheet()->getStyle("C3:O6")->applyFromArray($style);


        $gdImage = imagecreatefromjpeg('images/principal1.jpg');
// Add a drawing to the worksheetecho date('H:i:s') . " Add a drawing to the worksheet\n";
        $objDrawing = new PHPExcel_Worksheet_MemoryDrawing();
        $objDrawing->setName('Principal-1');
        $objDrawing->setDescription('Principal-1');
        $objDrawing->setImageResource($gdImage);
        $objDrawing->setRenderingFunction(PHPExcel_Worksheet_MemoryDrawing::RENDERING_JPEG);
        $objDrawing->setMimeType(PHPExcel_Worksheet_MemoryDrawing::MIMETYPE_DEFAULT);
        $objDrawing->setHeight(180);
        $objDrawing->setCoordinates('A1');
        $objDrawing->setWorksheet($objPHPExcel->getActiveSheet());


        $gdImage2 = imagecreatefromjpeg('images/principal2.jpg');
// Add a drawing to the worksheetecho date('H:i:s') . " Add a drawing to the worksheet\n";
        $objDrawing2 = new PHPExcel_Worksheet_MemoryDrawing();
        $objDrawing2->setName('Principal-2');
        $objDrawing2->setDescription('Principal-2');
        $objDrawing2->setImageResource($gdImage2);
        $objDrawing2->setRenderingFunction(PHPExcel_Worksheet_MemoryDrawing::RENDERING_JPEG);
        $objDrawing2->setMimeType(PHPExcel_Worksheet_MemoryDrawing::MIMETYPE_DEFAULT);
        $objDrawing2->setHeight(180);
        $objDrawing2->setCoordinates('P1');
//        $objDrawing2->getOffsetX(100);
//        $objDrawing2->getOffsetY(100);
        $objDrawing2->setWorksheet($objPHPExcel->getActiveSheet());
    }

    if ($reportType == 4) {
        $objPHPExcel->getActiveSheet()->getStyle($celdastitulo)->applyFromArray($styleArray);
        unset($styleArray);

        $objPHPExcel->getActiveSheet()->getStyle($celdastitulo)->applyFromArray($style);

        $range = "A11:F" . ($numcols + 10);
        $objPHPExcel->getActiveSheet()->getStyle($range)->applyFromArray($styleArray2);
        unset($styleArray2);

        $objPHPExcel->getActiveSheet()->mergeCells('A8:D8');
        $objPHPExcel->getActiveSheet()->mergeCells('C3:F3');
        $objPHPExcel->getActiveSheet()->mergeCells('C5:F5');
        $objPHPExcel->getActiveSheet()->mergeCells('C6:F6');

        $objPHPExcel->getActiveSheet()->SetCellValue('C3', 'DISTRIBUCIONES LA PRINCIPAL LA GRITA, C.A.');
        $objPHPExcel->getActiveSheet()->SetCellValue('C5', 'Reporte Cantidad de Despachos por Fletero');
        $objPHPExcel->getActiveSheet()->SetCellValue('C6', 'Sucursal La Grita');
        $objPHPExcel->getActiveSheet()->SetCellValue('A8', 'Fecha de Reporte: '.$fecha);
        $objPHPExcel->getActiveSheet()->getStyle('A8:D8')->getFont()->setBold(true);

        $objPHPExcel->getActiveSheet()->getStyle('C3')->applyFromArray($font1);
        $objPHPExcel->getActiveSheet()->getStyle('C5')->applyFromArray($font2);
        $objPHPExcel->getActiveSheet()->getStyle('C6')->applyFromArray($font3);

        $objPHPExcel->getActiveSheet()->getStyle("C3:F6")->applyFromArray($style);

//Agregar Imagen al Archivo
        $gdImage = imagecreatefromjpeg('images/principal1.jpg');
// Add a drawing to the worksheetecho date('H:i:s') . " Add a drawing to the worksheet\n";
        $objDrawing = new PHPExcel_Worksheet_MemoryDrawing();
        $objDrawing->setName('Principal-1');
        $objDrawing->setDescription('Principal-1');
        $objDrawing->setImageResource($gdImage);
        $objDrawing->setRenderingFunction(PHPExcel_Worksheet_MemoryDrawing::RENDERING_JPEG);
        $objDrawing->setMimeType(PHPExcel_Worksheet_MemoryDrawing::MIMETYPE_DEFAULT);
        $objDrawing->setHeight(180);
        $objDrawing->setCoordinates('A1');
        $objDrawing->setWorksheet($objPHPExcel->getActiveSheet());

        $gdImage2 = imagecreatefromjpeg('images/principal2.jpg');
// Add a drawing to the worksheetecho date('H:i:s') . " Add a drawing to the worksheet\n";
        $objDrawing2 = new PHPExcel_Worksheet_MemoryDrawing();
        $objDrawing2->setName('Principal-2');
        $objDrawing2->setDescription('Principal-2');
        $objDrawing2->setImageResource($gdImage2);
        $objDrawing2->setRenderingFunction(PHPExcel_Worksheet_MemoryDrawing::RENDERING_JPEG);
        $objDrawing2->setMimeType(PHPExcel_Worksheet_MemoryDrawing::MIMETYPE_DEFAULT);
        $objDrawing2->setHeight(180);
        $objDrawing2->setCoordinates('G1');
        $objDrawing2->setWorksheet($objPHPExcel->getActiveSheet());
    }

// Se modifican los encabezados del HTTP para indicar que se envia un archivo de Excel.
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
//    header('Content-Disposition: attachment;filename="' . $filename . ' ' . $fecha . '.xlsx"');
    header('Content-Disposition: attachment;filename="'.$filename.'.xlsx"');
    header('Cache-Control: max-age=0');
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    $objWriter->save('php://output');
    exit;
}