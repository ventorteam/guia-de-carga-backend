<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------
| DATABASE CONNECTIVITY SETTINGS
| -------------------------------------------------------------------
| This file will contain the settings needed to access your database.
|
| For complete instructions please consult the 'Database Connection'
| page of the User Guide.
|
| -------------------------------------------------------------------
| EXPLANATION OF VARIABLES
| -------------------------------------------------------------------
|
|	['dsn']      The full DSN string describe a connection to the database.
|	['hostname'] The hostname of your database server.
|	['username'] The username used to connect to the database
|	['password'] The password used to connect to the database
|	['database'] The name of the database you want to connect to
|	['dbdriver'] The database driver. e.g.: mysqli.
|			Currently supported:
|				 cubrid, ibase, mssql, mysql, mysqli, oci8,
|				 odbc, pdo, postgre, sqlite, sqlite3, sqlsrv
|	['dbprefix'] You can add an optional prefix, which will be added
|				 to the table name when using the  Query Builder class
|	['pconnect'] TRUE/FALSE - Whether to use a persistent connection
|	['db_debug'] TRUE/FALSE - Whether database errors should be displayed.
|	['cache_on'] TRUE/FALSE - Enables/disables query caching
|	['cachedir'] The path to the folder where cache files should be stored
|	['char_set'] The character set used in communicating with the database
|	['dbcollat'] The character collation used in communicating with the database
|				 NOTE: For MySQL and MySQLi databases, this setting is only used
| 				 as a backup if your server is running PHP < 5.2.3 or MySQL < 5.0.7
|				 (and in table creation queries made with DB Forge).
| 				 There is an incompatibility in PHP with mysql_real_escape_string() which
| 				 can make your site vulnerable to SQL injection if you are using a
| 				 multi-byte character set and are running versions lower than these.
| 				 Sites using Latin-1 or UTF-8 database character set and collation are unaffected.
|	['swap_pre'] A ventor table prefix that should be swapped with the dbprefix
|	['encrypt']  Whether or not to use an encrypted connection.
|
|			'mysql' (deprecated), 'sqlsrv' and 'pdo/sqlsrv' drivers accept TRUE/FALSE
|			'mysqli' and 'pdo/mysql' drivers accept an array with the following options:
|
|				'ssl_key'    - Path to the private key file
|				'ssl_cert'   - Path to the public key certificate file
|				'ssl_ca'     - Path to the certificate authority file
|				'ssl_capath' - Path to a directory containing trusted CA certificats in PEM format
|				'ssl_cipher' - List of *allowed* ciphers to be used for the encryption, separated by colons (':')
|				'ssl_verify' - TRUE/FALSE; Whether verify the server certificate or not ('mysqli' only)
|
|	['compress'] Whether or not to use client compression (MySQL only)
|	['stricton'] TRUE/FALSE - forces 'Strict Mode' connections
|							- good for ensuring strict SQL while developing
|	['ssl_options']	Used to set various SSL options that can be used when making SSL connections.
|	['failover'] array - A array with 0 or more data for connections if the main should fail.
|	['save_queries'] TRUE/FALSE - Whether to "save" all executed queries.
| 				NOTE: Disabling this will also effectively disable both
| 				$this->db->last_query() and profiling of DB queries.
| 				When you run a query, with this setting set to TRUE (ventor),
| 				CodeIgniter will store the SQL statement for debugging purposes.
| 				However, this may cause high memory usage, especially if you run
| 				a lot of SQL queries ... disable this to avoid that problem.
|
| The $active_group variable lets you choose which connection group to
| make active.  By ventor there is only one group (the 'ventor' group).
|
| The $query_builder variables lets you determine whether or not to load
| the query builder class.
*/
$active_group = 'ventor';

$query_builder = TRUE;

/*$db['ventor'] = array(
	'dsn'	=> '',
	'hostname' => '190.8.170.115',
	'username' => 'root',
	'password' => 'VENTORSQL',
	'database' => 'ventoradm001',
	'dbdriver' => 'mysqli',
	'dbprefix' => '',
	'pconnect' => FALSE,
	'db_debug' => (ENVIRONMENT !== 'production'),
	'cache_on' => FALSE,
	'cachedir' => '',
	'char_set' => 'utf8',
	'dbcollat' => 'utf8_general_ci',
	'swap_pre' => '',
	'encrypt' => FALSE,
	'compress' => FALSE,
	'stricton' => FALSE,
	'failover' => array(),
	'save_queries' => TRUE
);*/
$db['ventor']['hostname'] = '192.168.128.2';
$db['ventor']['username'] = 'root';
//$db['ventor']['password'] = 'VENTORSQL';
$db['ventor']['password'] = 'kapauzna';
$db['ventor']['database'] = 'ventoradm001';
$db['ventor']['dbdriver'] = 'mysqli';
$db['ventor']['dbprefix'] = '';
$db['ventor']['pconnect'] = FALSE; # Recomendado para poder trabajar con ambas conexiones en paralelo
$db['ventor']['db_debug'] = TRUE;
$db['ventor']['cache_on'] = FALSE;
$db['ventor']['cachedir'] = '';
$db['ventor']['char_set'] = 'utf8';
$db['ventor']['dbcollat'] = 'utf8_general_ci';
$db['ventor']['swap_pre'] = '';
$db['ventor']['autoinit'] = TRUE;
$db['ventor']['stricton'] = FALSE;
//$db['ventor']['port'] = 3306;
$db['guia']['port'] = 3308

// Nuestra segunda base de datos 190.8.170.115:
 +
$db['guia']['hostname'] = '192.168.128.2';
$db['guia']['username'] = 'root';
$db['guia']['password'] = 'kapauzna';
//$db['guia']['password'] = 'VENTORSQL';
$db['guia']['database'] = 'guiacarga';
$db['guia']['dbdriver'] = 'mysqli';
$db['guia']['dbprefix'] = '';
$db['guia']['pconnect'] = FALSE; # Recomendado para poder trabajar con ambas conexiones en paralelo
$db['guia']['db_debug'] = TRUE;
$db['guia']['cache_on'] = FALSE;
$db['guia']['cachedir'] = '';
$db['guia']['char_set'] = 'utf8';
$db['guia']['dbcollat'] = 'utf8_general_ci';
$db['guia']['swap_pre'] = '';
$db['guia']['autoinit'] = FALSE; # A partir de la segunda conexión, es recomendable dejar en FALSE este valor.
$db['guia']['stricton'] = FALSE;
//$db['guia']['port'] = 3306;
$db['guia']['port'] = 3308;